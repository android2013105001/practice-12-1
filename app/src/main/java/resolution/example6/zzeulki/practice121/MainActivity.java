package resolution.example6.zzeulki.practice121;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void show_map(View v){

        double longitude;
        double latitude;


        if(v.getId() == R.id.button ){
            longitude = 128.616869;
            latitude = 35.903164;
        }

        else if(v.getId() == R.id.button2){
            longitude = 128.611350;
            latitude = 35.890313;
        }
        else if(v.getId() == R.id.button3){
            longitude = 128.609626;
            latitude = 35.887057;
        }
        else
        {
            longitude = 128.609696;
            latitude = 35.888301;
        }

        String pos = String.format("geo:%f,%f? z = %d ",latitude,longitude,18);
        Uri geo = Uri.parse(pos);
        Intent intent = new Intent(Intent.ACTION_VIEW,geo);
        startActivity(intent);
    }
}
